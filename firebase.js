import { initializeApp } from "https://www.gstatic.com/firebasejs/9.6.3/firebase-app.js";
import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.6.3/firebase-analytics.js";
import { getAuth, GoogleAuthProvider, signInWithPopup } from "https://www.gstatic.com/firebasejs/9.6.3/firebase-auth.js";
import { getDatabase } from "https://www.gstatic.com/firebasejs/9.6.3/firebase-database.js";
import { getFirestore } from "https://www.gstatic.com/firebasejs/9.6.3/firebase-firestore.js";

const firebaseConfig = {
  apiKey: "AIzaSyCGYLSyszqzD3xhqe9mSPISerlpKrnniuY",
  authDomain: "cloudsprojecteurecom.firebaseapp.com",
  projectId: "cloudsprojecteurecom",
  storageBucket: "cloudsprojecteurecom.appspot.com",
  messagingSenderId: "840624940109",
  appId: "1:840624940109:web:4118ff96071c11e790e804",
  measurementId: "G-R3739RYR44"
};


const app = initializeApp(firebaseConfig); 
const analytics = getAnalytics(app);

const provider = new GoogleAuthProvider();
provider.setCustomParameters({ prompt: "select_account" });

export const auth = getAuth();
export const signInWithGoogle = () => signInWithPopup(auth, provider);
export const db = getDatabase(app);
export const dbFirestore = getFirestore(app);
